#!/usr/bin/env python3



import todo.todo as todo


# Moved the usage for this file


choose_opt = ""

while choose_opt != 8:
    print("Choose your option\n")
    list_opt = """\
    1 Create a new List
    2 Show the lists
    3 Delete a list
    4 Add a task
    5 Delete a task
    6 Mark task done
    7 Show tasks of list
    8 Quit
    """

    print(list_opt)
    choose_opt = int(input("Enter a number of option: "))

    match choose_opt:
        case 1:
            choose_name         = str(input("Enter a name: "))

            try:
                todo.new_list(choose_name)
            except:
                print("Error")

        case 2:
            try:
                lists           = todo.show_list()
                for i in lists:
                    print(i[0])
            except:
                print("Error")

        case 3:
            choose_del          = str(input("Enter a name of list: "))

            try:
                todo.del_list(choose_del)
            except:
                print("Error")

        case 4:
            choose_add_task     = str(input("Enter a name for a new task: "))
            choose_name_list    = str(input("Enter a name of list: "))

            try:
                todo.add_task(choose_name_list, choose_add_task)
            except:
                print("Error")

        case 5:
            choose_del_task     = str(input("Enter a name of task for delete: "))
            choose_name_list    = str(input("Enter a name of list: "))

            try:
                todo.del_task(choose_name_list, choose_del_task)
            except:
                print("Error")

        case 6:
            choose_task_ok      = str(input("Enter a name of task: "))
            choose_name_list    = str(input("Enter a name of list: "))

            try:
                todo.task_ok(choose_name_list, choose_task_ok)
            except:
                print("Error")

        case 7:
            lists               = todo.show_list()

            print("\nChoose a list\n")
            if len(lists) > 0:
                for i in lists:
                    print(i[0])
                choose_name_list    = str(input("Enter a name of list: "))
                lists_tasks         = todo.show_tasks(choose_name_list)

                print(f"{'+'*68}".format())
                print(f"| State  | Task {' '*50} |".format())
                print(f"{'+'*68}".format())

                for i in lists_tasks:

                    if i[1] == 'FALSE':
                        i_status = 'To Do'
                    elif i[1] == 'TRUE':
                        i_status = 'Done'

                    print(f"| {i_status:7}| {i[0]:55} |".format())

                print(f"{'+'*68}".format())
            else:
                print("Not found ToDo\'s")

        case _:
            print("Option not found")
            print("Exiting of program")

