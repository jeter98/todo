#!/usr/bin/env python3

"""
Project of a To Do list in Python
For this project i used the SQLite3 library
"""



# Importing necessary libraries

import sqlite3
import os



# Variables 

home_dir        =   os.getenv('HOME')
todo_dir        =   home_dir+'/todo'
db_path         =   home_dir+'/todo/todo_db.db'



# Create directory and file database of todo

try:
    os.mkdir(todo_dir)
except:
    pass



# Functions

def new_list(name):
    """
    This function will create a new ToDo list
    """
    
    # Connecting to the database
    connection = sqlite3.connect(db_path)

    # SQL
    sql  =  f"CREATE TABLE IF NOT EXISTS '{name}' (".format()
    sql +=  f"task TEXT,".format() 
    sql +=  f"done REAL)".format()

    # Executing the query
    cursor_con          = connection.cursor()
    cursor_con.execute(sql)
    connection.commit()

    # Closing the cursor and connection
    cursor_con.close()
    connection.close()



def show_list():
    """
    This function will show the existing ToDo list
    """

    # Connection to the database
    connection          = sqlite3.connect(db_path)

    # SQL
    sql = f"SELECT name FROM sqlite_schema WHERE type='table'".format()

    # Executing the sql
    cursor_con          = connection.cursor()
    sql_response        = cursor_con.execute(sql)
    response_fetch      = sql_response.fetchall()

    # Closing the cursor and connection
    cursor_con.close()
    connection.close()

    # Return of function
    return response_fetch



def del_list(name):
    """
    This function will delete a ToDo list
    """

    # Connectin to the database
    connection          = sqlite3.connect(db_path)

    # SQL
    sql  = f"DROP TABLE IF EXISTS '{name}'".format()

    # Executing the sql
    cursor_con          = connection.cursor()
    cursor_con.execute(sql)
    connection.commit()

    # Closing the cursor and connection
    cursor_con.close()
    connection.close()



def add_task(name, task):
    """
    This function will add a task in the list
    """
    
    # Connecting to the database
    connection          = sqlite3.connect(db_path)

    # SQL
    sql  = f"INSERT INTO '{name}' (task, done) ".format()
    sql += f"VALUES ('{task}', 'FALSE')".format()
    
    # Executing the sql
    cursor_con          = connection.cursor()
    cursor_con.execute(sql)
    connection.commit()

    # Closing the cursor and connection
    cursor_con.close()
    connection.close()



def show_tasks(name):
    """
    This function will show the tasks of a particular list
    """

    # Connecting to the databse
    connection          = sqlite3.connect(db_path)

    # SQL
    sql = f"SELECT * FROM '{name}'".format()

    # Executing the sql
    cursor_con          = connection.cursor()
    sql_response        = cursor_con.execute(sql)
    response_fetch      = sql_response.fetchall()

    # Closing the cursor and connection
    cursor_con.close()
    connection.close()

    # Return of function
    return response_fetch



def del_task(name, task):
    """
    This function will delete a task of list
    """
    
    # Connection to the database
    connection = sqlite3.connect(db_path)

    # SQl
    sql = f"DELETE FROM '{name}' WHERE task = '{task}'".format()

    # Executing the sql
    cursor_con          = connection.cursor()
    cursor_con.execute(sql)
    connection.commit()

    # Closing the cursor and connection
    cursor_con.close()
    connection.close()



def task_ok(name, task):
    """
    This function marks the tas as it is ok
    """

    # Connection to the database
    connection = sqlite3.connect(db_path)

    # SQl
    sql = f"UPDATE '{name}' SET done = 'TRUE' WHERE task = '{task}'".format()

    # Executing the sql
    cursor_con          = connection.cursor()
    cursor_con.execute(sql)
    connection.commit()

    # Closing the cursor and connection
    cursor_con.close()
    connection.close()

