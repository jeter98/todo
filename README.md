# Todo


## ToDo list 

todo is a simple example of a todo list writed in python
and use the library SQLite3 to store tasks.


## Usage
For use this program type
```
cd todo
python3 usage.py
```

Chosse the option 
```
    1 Create a new List
    2 Show the lists
    3 Delete a list
    4 Add a task
    5 Delete a task
    6 Mark task done
    7 Show tasks of list
```
